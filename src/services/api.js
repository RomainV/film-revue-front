import * as axios from "axios";

export default class Api {
    constructor() {
        this.api_token = null;
        this.client = null;
        this.api_url = "http://localhost:3001";
    }

    init = () => {

        this.client = axios.create({
            baseURL: this.api_url,
            timeout: 3000,
        });

        return this.client;
    };

    getUserList = () => {
        return this.init().get("/users");
    };

    addNewUser = (data) => {
        return this.init().post("/users", data);
    };
}
