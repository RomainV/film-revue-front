import './App.css';

import React, {useState} from 'react';
import './index.css';
import UsersList from "./components/user/UsersList";
import NavbarWrapper from "./components/navbar/NavbarWrapper";
import {BrowserRouter as Router, Route, useHistory,} from "react-router-dom";
import Films from "./components/Films";
import {Constantes} from "./components/constantes/routes";
import NewFilm from "./components/NewFilm/newFilm";

function App () {
    const [currentUser, setCurrentUser] = useState(null);
    const history = useHistory();

    const isLoggedIn = currentUser != null;
    // let display;
    if (!isLoggedIn) {
        history.push(Constantes.ROUTE_CONNEXION);
    }

    return (
        <div className="App">
            <Router>
                <NavbarWrapper currentUser={currentUser} changeUser={setCurrentUser}/>
                <Route path={Constantes.ROUTE_CONNEXION}>
                    <UsersList userChange={setCurrentUser} />
                </Route>
                <Route path={Constantes.ROUTE_LISTE}>
                    <Films currentUser={currentUser} />
                </Route>
                <Route path={Constantes.ROUTE_NEW_FILM}>
                    <NewFilm currentUser={currentUser} />
                </Route>
            </Router>
            {/*<UsersList userChange={setCurrentUser} />*/}
            {/*<Router>*/}
            {/*    <Route path="/liste" component={FilmsList} />*/}
            {/*</Router>*/}
            {/*{display}*/}
        </div>
    );
}

export default App;
