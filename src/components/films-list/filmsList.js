import React, {useEffect, useState} from 'react';
import './filmsList.css'
import {Constantes} from "../constantes/routes";
import {useHistory} from "react-router-dom";

function FilmsList() {
    const [filmsList, setFilmList] = useState([]);

    const history = useHistory();

    let seeFilmDetails = (film) => {
        console.debug('set route to ', `${Constantes.ROUTE_LISTE}/${film._id}`, ' with ', film);
        history.push(`${Constantes.ROUTE_LISTE}/${film._id}`);
    }

    useEffect(() => {
        fetch(`http://localhost:3001/films`)
            .then(res => res.json())
            .then(json => setFilmList(json));
    }, []);

    return (
        <div className="container-fluid liste-films">
            <h2 className="text-center">Liste des films</h2>
            <div className="film-card-container">
                <ul className="list-group">
                    {filmsList.map(film => (
                        <li key={film._id} className="list-group-item">
                            <img src={process.env.PUBLIC_URL + '/no_image.png'} alt="unavailable" className={"movies-list_image"}/>
                            <div onClick={() => seeFilmDetails(film)}>
                                <h2>{film.title}</h2>
                            </div>
                        </li>
                    ))}
                </ul>
            </div>
        </div>
    );
}

export default FilmsList;
