import React from 'react';
import './reviews.css'

class Reviews extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        const options = { year: 'numeric', month: 'long', day: 'numeric' };

        return (
            <div className="comment">
                <h3>{this.props.review.title}</h3>
                <p>{this.props.review.text}</p>
                <p>Note : {this.props.review.note} / 100</p>
                <h4>Avis de <strong>{this.props.review.owner.pseudo}</strong> - <span>
                            {this.props.review.publicationDate ? new Date(this.props.review.publicationDate).toLocaleDateString('fr-FR', options) : 'Pas de date de publication'}
                        </span>
                </h4>
            </div>
        );
    }
}

export default Reviews;
