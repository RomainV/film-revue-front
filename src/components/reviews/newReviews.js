import React from 'react';
import {FormControl, Form, Button} from "react-bootstrap";
import "./newReviews.css"

class NewReviews extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            title: '',
            text: '',
            note: '',
        };

        this.changeTitle = this.changeTitle.bind(this);
        this.changeText = this.changeText.bind(this);
        this.changeNote = this.changeNote.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    changeTitle(event) {
        this.setState({
            title: event.target.value
        });
    }
    changeText(event) {
        this.setState({
            text: event.target.value
        });
    }
    changeNote(event) {
        this.setState({
            note: event.target.value
        });
    }

    handleSubmit(event) {
        // Simple POST request with a JSON body using fetch
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                title: this.state.title,
                text: this.state.text,
                note: this.state.note,
                owner: this.props.user._id,
                reference: this.props.film._id,
                publicationDate: Date.now(),
            })
        };
        fetch('http://localhost:3001/reviews', requestOptions)
            .then(response => response.json())
            .then(() => this.props.newReview());

        event.preventDefault();
    }

    componentDidMount() {
        fetch(`http://localhost:3001/films`)
            .then(res => res.json())
            .then(json => this.setState({ filmsList: json }));
    }

    render() {
        return (
            <div>
                <div>
                    <h2 className="comment-new-title text-center">Rédiger un avis</h2>
                    <Form onSubmit={this.handleSubmit}>
                            <FormControl className="form-margin-bottom" placeholder="Titre" type="text" value={this.state.newFilmName} onChange={this.changeTitle} />
                            <FormControl className="form-margin-bottom" as="textarea" placeholder="Commentaire" value={this.state.text} onChange={this.changeText} />
                            <FormControl className="form-margin-bottom" placeholder="Note" type="number" value={this.state.note} onChange={this.changeNote} />
                        <Button type="submit" variant={"outline-primary"}>Envoyer</Button>
                    </Form>
                </div>
            </div>
        );
    }
}

export default NewReviews;
