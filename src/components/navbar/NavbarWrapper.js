import React from 'react';
import '../../index.css';
import './Navbar.css';
import {Button, Navbar} from "react-bootstrap";
import {useHistory} from "react-router-dom";

function NavbarWrapper(props) {

    const history = useHistory();

    const deconnexion = () => {
        history.push('/connexion');
        props.changeUser(null);
    }

    return (
        <Navbar bg="dark" variant="dark">
            <Navbar.Brand>Film-Review.Home</Navbar.Brand>
            <Navbar.Toggle />
            <Navbar.Collapse className="justify-content-end">
                <Navbar.Text>
                    Connecté en tant que: <span className="strong-text-navbar">{props.currentUser ? props.currentUser.pseudo : 'Anonyme'}</span>
                </Navbar.Text>
                {/* FIXME Pour avoir un espace dans le menu */}
                <Navbar.Brand />
                <Button variant="outline-secondary" onClick={() => deconnexion()}>Déconnexion</Button>
            </Navbar.Collapse>
        </Navbar>
    )
}


export default NavbarWrapper;
