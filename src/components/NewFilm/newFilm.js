import './NewFilm.css'
import React, {useState} from 'react';
import {Button, Form, FormControl, InputGroup} from "react-bootstrap";
import {Constantes} from "../constantes/routes";
import {DateTime} from 'luxon';
import {useHistory} from "react-router-dom";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCaretLeft} from "@fortawesome/free-solid-svg-icons";

function NewFilm() {
    let [newFilmName, setNewFilmName] = useState('');
    let [resultList, setResultList] = useState([]);

    let history = useHistory();

    const returnToList = () => {
        history.push(Constantes.ROUTE_LISTE);
    }

    let formatDate = (date) => {
        return DateTime.fromISO(date).toFormat("dd/MM/yyyy");
    }

    let handleSubmit = (event) => {
        // Simple POST request with a JSON body using fetch
        let headers = {};
        headers['Content-Type'] = 'application/json';
        // headers[Constantes.TRAKT_API_KEY_HEADER] = Constantes.TRAKT_API_KEY;
        // headers[Constantes.TRAKT_API_VERSION_HEADER] = Constantes.TRAKT_API_VERSION;

        let query = encodeURIComponent(newFilmName);

        const requestOptions = {
            method: 'GET',
            headers: headers,
        };
        fetch(`${Constantes.TMDB_API_SEARCH}?`
            + `${Constantes.TMDB_API_KEY_PARAMETER}=${Constantes.TMDB_API_KEY}`
            + `&${Constantes.TMDB_API_LANGUAGE_PARAMETER}=${Constantes.TMDB_API_LANGUAGE}`
            + `&${Constantes.TMDB_API_QUERY_PARAMETER}=${query}`, requestOptions).then(r => {
            // Get result
            console.debug('r', r);
            r.json().then(value => {
                console.debug('v', value);
                setResultList(value.results)
            });
        });
        event.preventDefault();
    }

    const selectFilm = (tmdb_id, title) => {
        const requestOptions = {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({
                tmdb_id: tmdb_id,
                title: title
            })
        };
        fetch(`${Constantes.API_PATH}/${Constantes.API_ROUTE_FILM}`, requestOptions).then(() => {
            history.push(Constantes.ROUTE_LISTE);
        })
    }

    return (
        <div className="container-fluid liste-films">
            <span onClick={() => returnToList()}>
                <FontAwesomeIcon icon={faCaretLeft}/> Retour à la liste de films
            </span>
            <Form onSubmit={handleSubmit} className={"search-form"}>
                <InputGroup>
                    <FormControl type="text" value={newFilmName} onChange={(e) => setNewFilmName(e.target.value)}/>
                    <InputGroup.Append>
                        <Button type="submit">Chercher</Button>
                    </InputGroup.Append>
                </InputGroup>
            </Form>
            <h2>Résultat de la recherche</h2>
            {resultList.map(result => (
                <div className="film-result" onClick={() => selectFilm(result.id, result.title)} key={result.id}>
                    <img className="film-image" src={
                        result.poster_path ?
                            Constantes.TMDB_IMAGE_API + Constantes.TMDB_IMAGE_API_SIZE + result.poster_path
                            : process.env.PUBLIC_URL + '/no_image.png'} alt={"poster"}/>
                    <div className="film-desc">
                        <h3>{result.title}</h3>
                        <span>{formatDate(result.release_date)}</span>
                        <p>{result.overview}</p>
                    </div>
                </div>
            ))}
        </div>
    );
}

export default NewFilm;
