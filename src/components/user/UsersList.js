import React, {useEffect, useState} from 'react';
import '../../index.css';
import './users-list.css'
import {Button, Card, Form, ListGroup} from "react-bootstrap";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes } from '@fortawesome/free-solid-svg-icons'
import { faCheckCircle} from "@fortawesome/free-solid-svg-icons";
import { useHistory } from "react-router-dom"
import {Constantes} from "../constantes/routes";


function UsersList(props) {

    const [usersList, setUsersList] = useState([]);
    const [newUserName, setNewUserName] = useState('');

    let history = useHistory();

    const changeName = (event) => {
        console.debug('event', props, history);
        history.push(Constantes.ROUTE_LISTE);
        props.userChange(event);
    }

    const handleSubmit = (event) => {
        // Simple POST request with a JSON body using fetch
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ pseudo: newUserName })
        };
        fetch(`${Constantes.API_PATH}/${Constantes.API_ROUTE_USER}`, requestOptions)
            .then(response => response.json())
            .then(data => {
                usersList.push(data);
                return setUsersList(usersList);
            });

        event.preventDefault();
    }

    const deleteUser = (pseudo) => {
        // Simple DELETE request fetch
        const requestOptions = {
            method: 'DELETE',
        };
        fetch(`${Constantes.API_PATH}/${Constantes.API_ROUTE_USER}/` + pseudo, requestOptions)
            .then( () => {
                usersList.splice( usersList.findIndex( (user) => user.pseudo === pseudo));
                return setUsersList( usersList);
            });
    }

    useEffect( () => {
        fetch(`${Constantes.API_PATH}/${Constantes.API_ROUTE_USER}`)
            .then(res => res.json())
            .then(json => setUsersList(json));
    }, []);
    
    return (
        <div className="connexion-wrapper">
            {/* Liste des utilisateurs */}
            <Card style={{ width: '18rem' }}>
                <Card.Title className="margin-title">Sélectionner votre nom</Card.Title>
                <ListGroup variant="flush">
                    {usersList.map(user => (
                        <ListGroup.Item key={user._id}>
                            <div className="userName" onClick={() => changeName(user)}>
                                {user.pseudo}
                                <div className="rightAlignUser"
                                     onClick={(e) => {e.stopPropagation();
                                         e.nativeEvent.stopImmediatePropagation(); deleteUser(user._id)}}>
                                    <FontAwesomeIcon icon={faTimes} />
                                </div>
                            </div>
                        </ListGroup.Item>
                    ))}
                    {/* Ajout d'un nouvel utilisateur */}
                    <ListGroup.Item>
                        <Form onSubmit={handleSubmit}>
                            <label>
                                <Form.Control type="text" placeholder="Nouvel utilisateur" value={newUserName} onChange={setNewUserName} />
                            </label>
                            <Button type="submit" className="rightAlignUser" variant={"outline-success"}><FontAwesomeIcon icon={faCheckCircle} /></Button>
                        </Form>
                    </ListGroup.Item>
                </ListGroup>
            </Card>
        </div>
    );
}


export default UsersList;
