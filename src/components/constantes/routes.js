
export class Constantes {
    static ROUTE_LISTE = "/film";
    static ROUTE_CONNEXION = "/connexion";
    static ROUTE_NEW_FILM = "/new";
    static ROUTE_FILM = "film/:id";

    static API_PATH = "http://localhost:3001";
    static API_ROUTE_USER = "users";
    static API_ROUTE_FILM = "films";
    static API_ROUTE_REVIEW = "reviews";

    static TRAKT_API = "https://api.trakt.tv/search/movie";
    static TRAKT_API_KEY_HEADER = "trakt-api-key";
    static TRAKT_API_KEY = "a4424ef5cac58d4a2bb74fe949ca8c27f86f86e4a7e3c9484f35d81191c43717";
    static TRAKT_API_VERSION_HEADER = "trakt-api-version";
    static TRAKT_API_VERSION = "2";

    static TMDB_API_SEARCH = "https://api.themoviedb.org/3/search/movie";
    static TMDB_API_MOVIE = "https://api.themoviedb.org/3/movie";
    static TMDB_IMAGE_API = "https://image.tmdb.org/t/p/";
    static TMDB_IMAGE_API_SIZE = "w154/";
    static TMDB_API_KEY_PARAMETER = "api_key";
    static TMDB_API_KEY = "c27a441e6281938199f5c213a109ef89";
    static TMDB_API_LANGUAGE_PARAMETER = "language";
    static TMDB_API_LANGUAGE = "fr-FR";
    static TMDB_API_QUERY_PARAMETER = "query";
}
