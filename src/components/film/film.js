import React, {useEffect, useState} from 'react';
import NewReviews from "../reviews/newReviews";
import '../reviews/reviews.css'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faCaretLeft} from "@fortawesome/free-solid-svg-icons";
import Reviews from "../reviews/reviews";
import {useHistory, useParams} from "react-router-dom";
import {Constantes} from "../constantes/routes";
import {DateTime} from "luxon";

function Film(props) {
    const [film, setFilm] = useState(null);
    const [filmFromApi, setFilmFromApi] = useState(null);

    const history = useHistory();
    const {id} = useParams();

    const returnToList = () => {
        history.push(Constantes.ROUTE_LISTE);
    }

    let formatDate = (date) => {
        return DateTime.fromISO(date).toFormat("dd/MM/yyyy");
    }

    const getFilmDetails = () => {
        let headers = {};
        headers['Content-Type'] = 'application/json';

        const requestOptions = {
            method: 'GET',
            headers: headers,
        };
        fetch(`${Constantes.TMDB_API_MOVIE}/${film.tmdb_id}?`
            + `${Constantes.TMDB_API_KEY_PARAMETER}=${Constantes.TMDB_API_KEY}`
            + `&${Constantes.TMDB_API_LANGUAGE_PARAMETER}=${Constantes.TMDB_API_LANGUAGE}`
            , requestOptions).then(r => {
            // Get result
            console.debug('r', r);
            r.json().then(value => {
                console.debug('v', value);
                setFilmFromApi(value);
            });
        });
    }

    const refreshReview = () => {
        fetch(`http://localhost:3001/films/${id}/reviews`)
            .then(res => res.json())
            .then(json => {
                setFilm( json );
            });
    }

    useEffect( () => {
        refreshReview();
    }, [])

    useEffect(() => {
        if ( film != null ) {
            getFilmDetails();
        }
    }, [film, setFilm])

    if ( film == null || filmFromApi == null ) {
        return (<div>Loading</div>);
    } else {
        return (
            <div>
                <span onClick={() => returnToList()}>
                    <FontAwesomeIcon icon={faCaretLeft}/> Retour à la liste de films
                </span>
                <h2>Aperçu du film</h2>
                <h3>{filmFromApi.title}</h3>
                <span>{formatDate(filmFromApi.release_date)}</span>
                <span>{filmFromApi.tagline}</span>
                <p>{filmFromApi.overview}</p>
                <h2>Avis</h2>
                <div className="comment">
                    <NewReviews film={film} user={props.currentUser} newReview={refreshReview}/>
                </div>
                {(film && film.reviews) ? film.reviews.map(review => (
                    <Reviews review={review} key={review._id}/>
                )) : <p>Encore aucun avis</p>}
            </div>
        );
    }
}

export default Film;
