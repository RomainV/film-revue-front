import React from 'react';
import '../index.css';
import FilmsList from "./films-list/filmsList";
import {Button} from "react-bootstrap";
import {Link, Route} from "react-router-dom"
import {Constantes} from "./constantes/routes";
import Film from "./film/film";
import {Switch} from "react-router";

function Films({currentUser}) {
    return (
        <div>
            <Switch>
                <Route path="/film/:id/">
                    <Film currentUser={currentUser}/>
                </Route>
                <Route path="/film">
                    <FilmsList currentUser={currentUser}/>
                    <Button><Link to={Constantes.ROUTE_NEW_FILM}>Ajouter un film</Link></Button>
                </Route>
            </Switch>
        </div>
    );
}

export default Films;
